"use strict"
// forward slash is use to comment
console.log("helloworld");

//variable declaration
var name = "harry";

//print the var
console.log(name);

//change the value of the variable name
name="potter";
console.log(name);

//rules for naming variable
//no number in the start
//underscore and dollar can be used in the start
//first_name :It is snake case writing
//firstName: It is camel case writing

//let keyword :
//declare a variable with keyword let

let firstName="HarryPotter";
console.log(firstName);
//to change the value of the variable
firstName="Potter";
console.log(firstName);

//declare constants
const pi=3.14;
//we cannot change the value of pi here like: pi="3.21"
console.log(pi*2);

//string indexing
let fName="Harry";
console.log(fName[0]);
//to find the length of the string
//it also counts the spaces in between, start and end
console.log(fName.length);
console.log(fName[fName.length-1]);

//some methods like:trim() 
//trim():It removes the space from the start and end of the string

let lName="   Potter   ";
console.log(lName.length);
console.log(lName.trim());


let newName= lName.trim();
console.log(newName);
console.log(newName.length);

lName= lName.trim();
console.log(lName);
console.log(lName.length);

//toUpperCase()
console.log(fName.toUpperCase());
fName=fName.toUpperCase();
console.log(fName);

//toLowerCase
fName=fName.toLowerCase();
console.log(fName);

//slice
//start index
//end index
let newString=fName.slice(0,3);
console.log(newString);
let string1=fName.slice(1,3);
console.log(string1);
let string2=fName.slice(1);
console.log(string2);

//typeof operator : it tells what datatype is present in variable
//string "Harry",numbers, booleans,undefined,null,BigInt these are all primitive datatypes.
let age= 12;
let firstname="Harry";
console.log(typeof age);

//convert number to string
age=age +""; //"22"
console.log(typeof age);

//convert string to number
let myStr="23";
console.log(typeof myStr);
myStr=+"23";
console.log(typeof(myStr));

let num="23";
num=Number(num);
console.log(typeof(num));

//string concatenation
let str1="Harry";
let str2="Potter";
let fullName=str1 + " " + str2;
console.log(fullName);

let num1="14";
let num2="13";
let total=num1+num2;
console.log(total);
console.log(typeof(total));

let num3="14";
let num4="13";
let total1=+num1+ +num2;
console.log(total1);
console.log(typeof(total1));

//template string

let num6=22;
let str_1="Harry";
// let aboutme="My name is "+str_1+" and my age is "+num6;
// console.log(aboutme);

let aboutme = `My name is ${str_1} and my age is ${num6}`;
console.log(aboutme);

//undefined
let name1;
console.log(typeof name1);
var name2;
console.log(typeof name2);
//both give the output undefined.
//if we give  only const pi;it will show error as the const should be always assigned a value.

//null

let myvar=null;
console.log(myvar);

// if we give console.log(typeof null); we get the ouput as object

//BigInt
let mynum=BigInt(123);
console.log(mynum);
console.log(Number.MAX_SAFE_INTEGER);//it tells us thst safely how big integer we can store
let mynum2=123n;
console.log(mynum+mynum2);
//here the we can add only if both hte numbers are BigInt
//here adding "n" to a number makes it bigint

//booleans and comparison operator
//booleans:true or false
let num5=2;
let num7=3;
console.log(num5>num6);

num5=3;
num7=3;
console.log(num5==num7);

//== vs ===
num5="3";
num7=3;
console.log(num5==num7);
//it gives output as true as == will check only the number and not the datatype
console.log(num5===num7);
//it gives output as false as === will check both datatype and value

//!= vs !==
console.log(num5 != num7);
//it gives output as false as it checks only values
console.log(num5 !== num7);
//it gives output as true as it checks both the value and the datatype

//truthy and falsy values

//if else condition
let age1=18;
if(age>18){
    console.log("User can play ddlc");
}
else{
    console.log("USer can play mario");
}
  
let num11=13;
if(num%2===0){
    console.log("even");
}
else{
    console.log("odd");
}

//falsy values
//false,"",null, undefined,0

let firstName1=""

if(firstName1)
{
    console.log(firstName1);
}
else{
    console.log("firstName kinda empty");

}
//the output is "firstName kinda empty" for firstName="false,"" null , undefined and 0"

//truthy values
//abc,-1,1;

//ternary operator

let age12=8;
let drink = age>=5 ? "coffee" :"milk";
console.log(drink);

//and or operators
firstName="harry";

if (age12===8 && firstname[0]==="h"){
    console.log("inside if");
}

if (age12===8 || firstname[0]==="h"){
    console.log("inside if");
}


//nested if else
//prompt takes the user input in the form of string
let winningNumber=19;
let userGuess= +prompt("Guess a number");
if(userGuess === winningNumber){
    console.log("Your guess is right");
}
else{
    if(userGuess<winningNumber){
        console.log("too low");
    }
    else{
        console.log("too high");
    }
}

//if 
//else if 
//else

let tempInDegree=15;

if(tempInDegree < 0){
    console.log("extremely cold outside");
}
else if(tempInDegree<16){
    console.log("It is cold outside");
}
else if(tempInDegree<25){
    console.log("lets go to swim");
}
else if(tempInDegree<35){
    console.log("turn on AC");
}
else{
    console.log("too hot");
}

//switch statememt
let day=2;
switch(day){
    case 0:console.log("Sunday");
            break;
    case 1:console.log("Monday");
            break;
    case 1:console.log("Tuesday");
            break;
    case 1:console.log("Wednesday");
            break;
    case 1:console.log("Thursday");
            break;
    case 1:console.log("Friday");
            break;
    case 1:console.log("Saturday");
            break;
    default:console.log("Not a valid day");
}

//while loop
let i=0;
while(i<10){
    console.log(i);
    i++;
}

let number=0;
let total11=0;
while(number<=10){
    total11=total+number;
    number++;
}

//for loop

for (let i=0;i<=9;i++){
    console.log(i);
}

//break keyword

for(let i=1;i<=10;i++){
    if(i==4){
        break;
    }
    console.log(i);
}

//continue keyword

for(let i=1;i<=10;i++){
    if(i==4){
        continue;
    }
    console.log(i);
}

//do while loop
let a=0;
do {
    console.log(i);
}while(i<=9);

//Arrays
let fruits=["apple","banana","grapes"];
console.log(fruits[0]);
let mixed=[1,2,3,"string"];

//To change the values in array
fruits[1]="mango";
console.log(fruits);

//function to check whether fruits is array
console.log(Array.isArray(fruits));
//output is true
let obj={};
console.log(Array.isArray(obj));
//output is false

//array push
fruits.push("banana");
console.log(fruits);
//array pop
fruits.pop();
console.log(fruits);

//unshift:adds value to the start
fruits.unshift("pear");
console.log(fruits);

//shift:delete the value from the start
fruits.shift();
console.log(fruits);

//primitive and reference data types
let num13=6;
let num14=num13;
console.log("value of num13: ",num13);
console.log("value of num14 is:",num14);
num13++;
console.log("after incrementing num13:",num13);
console.log("value is num13 is",num13);
console.log("value is num14 is",num14);

//reference types
let array1=["item1","item2"];
let array2=array1;
console.log("array1",array1);
console.log("array2",array2);
array1.push("item3");
console.log("after pushing element to array1",array1);
console.log("array1",array1);
console.log("array2",array2);

//array cloning
let array1=["item1","item2"];
let array2=["item1","item2"];

//array cloning using slice,concate and ... methods
let array2=array1.slice(0);
let array2=[].concat(array1);
let array2=[...array1];

console.log(array1===array2);
console.log(array1);
console.log(array2);

//for loop in arrays
let fruits=["apple","banana","mango","grapes"];
const fruits2=[];
for(let i=0;i<fruits.length;i++){
    console.log(fruits[i]);
    console.log(fruits[i].toUpperCase());
}

for (let fruit of fruits){
    console.log(fruit);
}

for(let fruit of fruits){
    fruits2.push(fruit.toUpperCase());
}

for(let i=0;i<fruits.length;i++){
    console.log(fruits[i]);
}

//use of const for creating array
const fruits=["apple","grapes"];
fruits.push("mango");
console.log(fruits);

//use of while loop in array
let j=0;
while(i<fruits.length){
    console.log(fruits[i]);
    i++;
}

//destructuring array
const myarray=["value1","value2","value3","value4"];
let [myvar1,myvar2, ...mynewArray]=myarray;
console.log("value of myvar1", myvar1);
console.log("value of myvar2",myvar2);
console.log(myNewArray);

//to create objects
const person={name:"Harshit",age:22};
console.log(person);

//access data from objects
console.log(person.name);
console.log(person.age);
console.log(person["age"]);

//how to add key value pair to objects
person.gender="male";
console.log(person);

//for in loop
const person={
    name:"harry",
    age:22,
    
}
for (let key in person){
    console.log(person[key]);
    console.log(`${key}:${person[key]}`);
}

for(let key of Object.keys(person)){
    console.log(person[key]);
}

//computed properties

const key1="color1";
const key2="color2";

const value1="red";
const value2="blue";

const obj={
    [key1]:value1,
    [key2]:value2
}
console.log(obj);

obj[key1]:value1;
obj[key2]:value2;
console.log(obj);

//spread operator
const array12=[1,2,3];
const array22=[5,6,7];

const newArray=[...array12, ...array22];
console.log(newArray);

//clone object
const obj1={
    key1:"value1";
    key2="value2";
}

const obj2={
    key3:"value3";
    key4:"value4";
};

const newObject={...obj1 };
console.log(newObject);

//object destructuring

const band={
    bandName:"Twin strings",
    famousSong:"allsongs",
};
let {bandName=var1,famousSong=var2}=band;
console.log(var1);

//objects inside an array
const users=[
    {userId:1,firstname:'Harry',gender:'male'},
    {userId:2,firstname:'heromine',gender:'female'},
    {userId:3,firstname:'potter',gender:'male'},
]
for(let user of users){
    console.log(user.firstname);
}

//nested destructuring
const [{firstname:user1firstname},{gender:user3gender}]=users;
console.log(user1firstname);
console.log(user3gender);
console.log(userId);

//functions

function singsong(){
    console.log("happy journey....");
}

function add(m,n){
    return m+n;
}

const sum=add(3,2);
console.log(sum);

//function for odd or even

function isodd(num){
    if (num%2===0){
        return false;
    }
    else{
        return true;
    }
}
console.log(isodd(3));

//function to target the array items if present
function findTarget(array,target){
    for(let i=0;i<array.length;i++){
        if(array[i]===target){
            return i;
        }
    }
    return -1;

}

const myArray=[1,4,5,6];
const output=findTarget(myArray ,4);
console.log(output);

//function expression 
const isEven(num){
    return num%2===0;

}
console.log(isEven(num));

//arrow functions
const totalsum =(num1,num2)=>{
    return num1+num2;

}
//const totalsum=(num1,num2)=> num1+num2; even this returns same value as above
const ans= totalsum(2,3);
console.log(ans);

//function inside the function
function app(){
    const myfunc=()=>{
        console.log("hello");
    }
    const sum=(num01,num02){
        return num01+num02;
    }
    const mul=(num01,num02)=>num01*num02;
    console.log("inside app");
    myfunc();
}
app();

//lexical scope
function myApp(){
    const myVar="value1";
    function myFunc(){
        const myVar="value22";
        console.log("inside myFunc",myVar);
    }
    
    console.log(myVar);
    myFunc();

}
myApp();

//block scope vs function scope

{
    let firstName="harry";
}
console.log(firstName);
//here we dont get the output because we can let in its block only

{ 
    var firstName="harry";

}
console.log(firstName);
//here let and const are block scope and var is function scope

//default parameters
function addnums(a,b=0){
    return a+b;
}
const sum1=addnums(4,2);
console.log(ans);

//rest parameters
function myFunc(a,b,...c){
    console.log(`a is ${a}`);
    console.log(`b is ${b}`);
    console.log(`c is ${c}`);

}
myFunc(1,2,3,4,5,6,3);

//param destructuring

const person={
    firstName:"harry",
    gender:"male",
}

function printDetails(firstName,gender,age){
    console.log(firstName);
    console.log(gender);
}
printDetails(person);

//callback functions

function myFunc2(name){
    console.log("inside my func 2");
    console.log(`your name is ${name}`)
}
function myFunc(callback){
    console.log("hello");
    callback("harry");
}

myFunc(myFunc2);

//function returning a function
function myFunc(){
    function greeting(){
        console.log("helloworld");
    }
    return greeting;

}
const answer=myFunc();
answer();

//array method:forEach
const numbers=[2,3,45,5];
function multiplyBy2(number,index){
    console.log(`index is ${index} number is ${number}`);
}
numbers.forEach(multiplyBy2);

//numbers.forEach(function(number,index)
//{
//   console.log(`index is ${index} number is ${number}`);
//});can be used instead of the above function

//using forEach and arrow function
const fruits=[
    {name:"mango",color:"yellow"},
    {name:"apple",color:"red"},
]
fruits.forEach((fruit)=>{
    console.log(fruit.name);
})

//map method:It makes new array
const numbers1=[3,4,5,2,1];

const square = function(number){
    return number*number;
}
const squarenum =numbers1.map((number)=>{
    return number*number;
});
console.log(squarenum);

//filter function

const oddnum=numbers1.filter((number)=>{
    return number % 2 !== 0;
});
console.log(oddnum);

//reduce method

const sumnumbs=numbers1.reduce((accumulator,currentValue)=>{
    return accumulator + currentValue;
});
console.log(sumnumbs);

//accumulator  , currentValue , return
//3               4              7
//7                5             12

const userCart=[
    {productId:1,productName:"mobile",price:12000},
    {productId:2,productName:"laptop",price:22000},
    {productId:3,productName:"tv",price:15000},
]
const totalAmount = userCart.reduce((totalPrice,currentproduct)=>{
    return totalPrice+currentproduct.price;

},0)
console.log(totalAmount);

//sort method
numbers1.sort();
console.log(numbers1);
numbers1.sort((a,b)=>{
    return a-b;
});
console.log(numbers1);

names=["abc","ABC","sherry"];
names.sort();
console.log(names);

//find method

const myArray1=["Hello","cat","dog","lion"];
function isLength(string){
    return string.length===3;
}
const answer1=myArray.find((string)=>string.length===3);
console.log(answer1);

const users1=[
    {userId:1, userName:"harry"},
    {userId:2, userName:"potter"},
    {userId:3, userName:"Sherry"},
    {userId:4, userName:"Perry"},
];

const myUser=users1.find((users)=> users.userId==3);
console.log(myUser);

//every method
function isEven(number){
    return number%2===0;
}
const output1= numbers1.every(isEven);
console.log(output1);

//from userCart
const ans1=userCart.every((cartItem)=>cartItem.price< 30000);
console.log(ans1);

//some method

const ans2=userCart.some((cartItem)=>cartItem.price>10000);
console.log(ans2);
//in numbers1 to check if any number is even and if any one number is even then prints true

//fill method
const myArray12=[1,2,3,4,5,6,7];
//value,start,end
myArray12.fill(0,2,5);
console.log(myArray12);

//splice method
//delete item
myArray12.splice(1,1);
//insert item
myArray12.splice(1,0,'inserted item');

//insert and delete
myArray12.splice(1,2,"item1","insert2");
console.log(myArray12);

//iterables:items on which for loop can be applied
const firstname1="harry";
for(let char of firstname1){
    console.log(char);
}

const items=['item1','item2','item3'];
for(let item of items){
    console.log(item);
}

//array like object :items which have length property and the items which can accessed through index
console.log(firstName1.length);
console.log(firstName1[1]);

//sets:no uplicate values allowed,has its own methods,no index-based access
const numbers2=new Set([1,2,3]);
console.log(numbers2);
numbers2.add(4);
console.log(numbers2);

//"has" in sets

if(numbers2.has(1)){
    console.log("1 is present");
}else{
    console.log("1 is not present");
}

const myArray123=[1,2,3,4,5,6,7];
const uniqueElements=new Set(myArray123);
console.log(uniqueElements);
console.log(myArray123);
let length=0;
for(let element of uniqueElements){
    length++;
}
console.log(length);

//map objects

//object literal
//key is always in string or symbol
//map always stores key value pairs

const person=new map();
person.set('firstName','Harry');
person.set('age',2);
person(1,'one');
console.log(person);
console.log(person.get(1));
for(let key of person.keys()){
    console.log(key,typeof key);
}

for (let [key, value] of perosn){
    console.log(Array.isArray(key));
    console.log(key, value);
}

const person1={
    id:1,
    firstName:"harry"
}
const userInfo=new Map();
userInfo.set(person1,{age:8,gender:"male"});
console.log(personal.id);
console.log(extraInfo.get(person1).age);

//clone using Object.assign

const obj00={
    key1:"value1",
    key2:"value2"
}

const obj01=object.assign({},obj);
obj.key3="value3";
console.log(obj);
console.log(obj2);

// optional chaining 
const user1  = {
    firstName: "harry",
}
console.log(user1.firstName);
console.log(user1.address.houseNumber);

// methods:function inside object
function personInfo(){
    console.log(`person name is ${this.firstName} and age is ${this.age}`);
}

const person12 = {
    firstName : "harry",
    age: 18,
    about: personInfo
}
const person22 = {
    firstName : "perry",
    age: 14,
    about: personInfo
}
const person33 = {
    firstName : "sherry",
    age: 17,
    about: personInfo
}
person12.about();
person22.about();
person33.about();

function about(hobby, music){
    console.log(this.firstName, this.age, hobby, music);
}
const user12={
    firstName:"harry",
    age: 12,   
}
const user22={
    firstName:"sherry",
    age: 14,
}

const user13={
    firstName:"harry",
    age:13,
    about:function(){
        console.log(this.firstName, this.age);
    }   
}

const myFunc = user13.about.bind(user13);
myFunc();

// arrow functions: in above example  we can add about in the following way 
//about: () => {
//       console.log(this.firstName, this.age);

// function to create an object

function createUser(firstName, lastName, email, age, address){
    const user12 = Object.created{createUser.prototype};
    user12.firstName = firstName;
    user12.lastName = lastName;
    user12.email = email;
    user12.age = age;
    user12.address = address;
    user12.about = function(){
        return `${this.firstName} is ${this.age} years old.`;
    };
    user12.is18 =  function(){
        return this.age >= 18;
    }
    user.sing=userMethods.sing;
    return user12;
}
createUser.prototype.about=function(){
    return `${this.firstName} is ${this.age} years old.`;
};
createUser.prototype.is18=function(){
    return this.age>=18;
}

const user14 = createUser('harry', 'potter', 'harrypotter@gmail.com', 12, "my address");
console.log(user1);
const is18 = user1.is18();
const about = user1.about();
console.log(about);


const user01=createUser('harry','potter','harrypotter@gmail.com',13,'hogworts');
const user02=createUser('helen','duck','helenduck@gmail.com',14,'berlin');
console.log(user01.about());
console.log(user02.sing());

const obj11={
    key1:"value1",
    key2:"value2"
}

const obj22={
    key3:"value3"
}
//to create an empty object
const obj02 =Object.create(obj11);
//__proto__,[[prototype]]
console.log(obj2.__proto__);



function hello(){
    console.log("helloworld");
}

//functions provide prototype property
hello.prototype.abc="abc";
hello.prototype.xyz="xyz";
console.log(hello.prototype.abc());

//new keyword
function createUser(firstName,age){
    this.firstname=firstname;
    this.age=age;
}
createUser.prototype.about=function(){
    console.log(this.firstName,this.age);
}
const newuser = new createUser("harry", 12);
newuser.about();

let numb12=new Array(1,2,3);
console.log(Object.getPrototypeOf(numbers));
console.log(Array.prototype);
console.log(numbers);

function hello1(){
    console.log("hello");
}
console.log(hello1.prototype);
hello1.prototype=[];

// class CreateUser{
//     constructor(firstName, lastName, email, age, address)
//         console.log("constructor called");
//         this.firstName = firstName;
//         this.lastName = lastName;
//         this.email = email;
//         this.age = age;
//         this.address = address;
    

//     about(){
//         return `${this.firstName} is ${this.age} years`;
//     }

//     is18(){
//         return this.age>=18;
//     }

//     sing(){
//         return "la la la";
//     }
    // func(a){
    //     console.log(a);
    // }
// }
// const user01=createUser('harry','potter','harrypotter@gmail.com',13,'hogworts');
// const user02=createUser('helen','duck','helenduck@gmail.com',14,'berlin');
// user1.func(12);

//super
class Animal{
    constructor(name, age){
        this.name=name;
        this.age=age;
    }
    eat(){
        return `${this.name} is eating`;
    }
    isSuperCute(){
        return this.age<=1;
    }
    isCute(){
        return true;
    }
}
class Dog extends Animal{
    constructor(name,age,speed){
        super(name,age);
        this.speed=speed;
    }
    eat(){
        return `Modified Eat: ${this.name} is eating`;
    }

    run(){
        return `${this.name} is running at ${this.speed}kmph`;
    }

}
const tommy=new Dog("tommy",3,45);
console.log(tommy.run());
console.log(tommy.eat());

//getter and setters
class Person{
    constructor(firstName,lastName,age){
        this.firstName=firstName;
        this.lastName=lastName;
        this.age=age;
    }
    //static methods and properties
    static classInfo(){
        return 'this is person class'
    
    }
    static desc="static property"
    get fullName(){
        return `${this.firstName} ${this.lastName}`
    }
    // setName(firstName,lastName){
    //     this.firstName=firstName;
    //     this.lastName=lastName;
    // }
    set fullName(fullName){
        const[firstName, lastName] = fullName.split(" ");
        this.firstName = firstName;
        this.lastName=lastName;

    }
}
const person01=new person("harry","potter",13);
console.log(person01.firstName);
console.log(person01.lastName);
person1.setName("helen","peters");
console.log(person01.firstName);
console.log(person01.lastName);
console.log(person01);
console.log(person01.fullName);

const info1=person01.classInfo();
console.log(person01.desc);
console.log(info1);





































